<html>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    HowTo Cards: Ensuring Integrity of Data Files with Checksums
  </title>
  <link rel="stylesheet" href="/website-test/style.css?build_nonce=build-1701769098" />
  <link rel="stylesheet" href="/website-test/fonts.css?build_nonce=build-1701769098" />
  <script>
  // settings for the provided by analytics.uni.lu (that drives the cookie banner)
  const settings = {
    expires: "180",
    matomoURL: "https://analytics.lcsb.uni.lu/hub/",
    siteID: "3",
    accept_all_text: "Aggregate statistics cookies accepted",
    only_necessary_text: "Only necessary cookies accepted",
    cookieName: "lap",
    bots: /bot|crawler|spider|crawling/i,
    timeout_hidebanner: "500",
    cookieDomain: window.location.hostname,
  }
  </script>
  <script src="https://analytics.lcsb.uni.lu/lap/static/js/lap.js"></script>
  <link rel="stylesheet" type="text/css" href="https://analytics.lcsb.uni.lu/lap/static/css/lap.css">
</head>
<body>
<header>
  <div class="flex-sidefill-l topbar-borderbottom"></div>
  <div class="width-sidebox-counterweight topbar-borderbottom"></div>
  <div class="width-content same-height">
    <div class="topbar-padding topbar-borderbottom"></div>
    <div class="topbar-unilu topbar-borderbottom">
      <a href="/website-test/"><div class="logo-unilu"></div></a>
    </div>
    <div class="topbar-text topbar-borderbottom">
      <div class="header-title">How-to Cards</div>
      <div class="header-subtitle">The practical and handy reference</div>
    </div>
    <div class="flex-fill topbar-borderbottom"></div>
    <div class="topbar-lcsb topbar-borderbottom">
      <div class="logo-lcsb"></div>
    </div>
    <div class="topbar-padding topbar-borderbottom"></div>
  </div>
  <div class="width-sidebox-placeholder topbar-borderbottom"></div>
  <div class="flex-sidefill-r topbar-borderbottom"></div>
</header>
<main>
  <div class="flex-sidefill-l"></div>
  <div class="content-holder">
    <div class="sidebox">
      <div class="sidebox-right">
        <a href="/website-test/">Home</a>
        |
        <a href="/website-test/tag">All categories</a>
        |
        <a href="/website-test/search">Search</a>
      </div>
      <div class="sidebox-header">
    Card categories
  </div>
  <div class="sidebox-values">
    <ul>
    <li class="sidebox-tag"><a href="/website-test/tag/data"> » All » Data management</a></li>
    <li class="sidebox-tag"><a href="/website-test/tag/integrity"> » All » Integrity</a></li>
    </ul>
  </div>
  <div class="sidebox-header">
    Outline
  </div>
  <div class="sidebox-values sidebox-toc">
    <ul>
<li><a href="#ensuring-integrity-of-data-files-with-checksums" id="toc-ensuring-integrity-of-data-files-with-checksums">Ensuring Integrity of Data Files with Checksums</a>
<ul>
<li><a href="#linux-and-macos" id="toc-linux-and-macos">Linux and macOS</a></li>
<li><a href="#windows" id="toc-windows">Windows</a></li>
</ul></li>
</ul>
  </div>
  <div class="sidebox-header">
    Last modification
  </div>
  <div class="sidebox-values">
    2023-10-27
  </div>
  
  <div class="sidebox-header">
    Update the card in GitLab
  </div>
  <div class="sidebox-values">
    <var>lcsb/howto/external</var> → <a href="https://gitlab.lcsb.uni.lu/lcsb/howto/external/-/blob/master/cards/integrity/checksum/checksum.md"><var>cards/integrity/checksum/checksum.md</var></a>
  </div>
  
    </div>
    <div class="content width-content">
    <h1 id="ensuring-integrity-of-data-files-with-checksums">Ensuring Integrity of Data Files with Checksums<a href="#ensuring-integrity-of-data-files-with-checksums" class="header-local-anchor" title="Link to this section">#</a></h1>
<p>Integrity of data files is critical for the verifiability of computational and lab-based analyses. The way to seal a data file’s content at a point in time is to generate a checksum. Checksum is a small sized datum generated by running an algorithm, called a cryptographic hash function, on a file. As long as a data file does not change, the calculation of the checksum will always result in the same datum. If you recalculate the checksum and it is different from a past calculation, then you know the file has been <strong>altered</strong> or <strong>corrupted</strong> in some way.</p>
<p>Below are typical situations that call for checksum generation:</p>
<ul>
<li>A data file has been newly downloaded or received from a collaborator.</li>
<li>You have copied data files to a new storage location, for instance you moved data from local computer to HPC to start an analysis.
You want to create a snapshot of your data, for instance when you’re creating a supplementary material folder for a paper/report. Note: If you snapshot your data by depositing it to a data repository. then typically checksum generation will be taken care of by the repository.</li>
</ul>
<p>In the remainder of this section we provide instructions for checksum generation on macOS, Windows and Linux platforms.</p>
<h2 id="linux-and-macos">Linux and macOS<a href="#linux-and-macos" class="header-local-anchor" title="Link to this section">#</a></h2>
<p>Command-line instructions for checksumming on Linux and macOS (Terminal) are common and is as follows.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">shasum</span> <span class="at">-a</span> 256 name_of_file</span></code></pre></div>
<p>Alternative to <strong>shasum</strong>, you may also use the commands <strong>md5sum</strong> (Linux) and <strong>md5</strong> (Mac).</p>
<p>An example execution of the <strong>shasum</strong> command is given below.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> shasum <span class="at">-a</span> 256  my_file.csv 0a1802c47c9c7fb29d8a6116dc40250c33321b56767125de332a862078570364  my_file.csv</span></code></pre></div>
<p>The recommended practice when generating checksums is to forward the checksum datum to a file, ideally with the same name as the data file. An example is given below.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> shasum <span class="at">-a</span> 256 my_file.csv <span class="op">&gt;</span> my_file.sha256</span></code></pre></div>
<p>The <em>.sha256</em> file extension denotes the algorithm that generated the checksum. Other common extensions are <em>.sha1</em> or <em>.md5</em>.</p>
<p>Given a data file and its checksum, one can <strong>verify</strong> the file against the checksum with the following command.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> shasum <span class="at">-c</span> my_file.sha256</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="ex">my_file.csv:</span> OK</span></code></pre></div>
<p>Finally, it is important to note that checksum calculation uses a file’s contents. When you create a copy of a file, the checksum calculator will generate the same datum for the copy. See an example below.</p>
<pre><code>$ shasum -a 256  my_file.csv
0a1802c47c9c7fb29d8a6116dc40250c33321b56767125de332a862078570364  my_file.csv
$ cp my_file.csv copy_file.csv
$ shasum -a 256 copy_file.csv
0a1802c47c9c7fb29d8a6116dc40250c33321b56767125de332a862078570364  copy_file.csv</code></pre>
<p>When you have several data files, checksum creation and verification needs to be automated. The following are free and open source utilities that can be used for this purpose:</p>
<ul>
<li><a href="https://github.com/JayBrown/Checksums">Checksums</a> macOS Automator workflow and shell script</li>
<li><a href="https://github.com/jessek/hashdeep/releases">md5deep</a> Command Line utility for recursive checksum operations (requires compilation on your platform)</li>
</ul>
<h2 id="windows">Windows<a href="#windows" class="header-local-anchor" title="Link to this section">#</a></h2>
<p>CertUtil is the command-line tool that Windows provides for checksum calculation. It is available in Windows Version 7 onwards.</p>
<p>To access CertUtil, from the <strong>Start</strong> menu select <strong>Command Prompt</strong>. Go to the folder containing your data file (my_file.csv) and run the following command:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="op">&gt;</span> certutil <span class="ex">-hashfile</span> my_file.csv SHA256</span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a><span class="ex">SHA256</span> hash of my_file.csv:</span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a><span class="ex">0a1802c47c9c7fb29d8a6116dc40250c33321b56767125de332a862078570364</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a><span class="ex">CertUtil:</span> <span class="at">-hashfile</span> command completed successfully.</span></code></pre></div>
<p>You can run the <strong>certutil</strong> command by changing the last parameter, <strong>SHA256</strong> to <strong>MD5</strong> or <strong>SHA512</strong>.
You can direct the result of checksum operation to a file as follows:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="op">&gt;</span> certutil <span class="ex">-hashfile</span> my_file.csv SHA256 <span class="op">&gt;</span> my_file.sha256</span></code></pre></div>
<p>CertUtil does not provide an option to automatically verify a given checksum against a file. Therefore, in order to do verification, you’d need to re-run the <strong>“certutil -hashfile …”</strong> command and manually compare the result with the earlier generated checksum.</p>
<p><a href="http://www.md5summer.org/">MD5Summer</a> is a free MD5 checksum tool for Windows. It is operated via a GUI and can perform recursive checksumming of files in folders. A step-by-step tutorial on using MD5Summer is provided by the UK Data Archive <a href="https://data-archive.ac.uk/media/361550/storingyourdata_checksumexercise.pdf">here</a>.</p>
      <div class="content-footer">
        <div class="flex-fill"></div>
        <div class="footer-r3"></div>
        <div class="footer-lcsb"></div>
        <div class="footer-text">
          <div>
            The contents are available under <a href="/website-test/license">CC-BY-SA 4.0 LICENSE</a>.
          </div>
          <div>
            <a href="/website-test/privacy-policy">Privacy Policy</a>
            (<span id="gdpr-result-text"></span> - <a href="javascript:%20showBanner();">change</a>)
          </div>
        </div>
        <div class="flex-fill"></div>
      </div>
    </div>
    <div class="width-sidebox-counterweight"></div>
  </div>
  <div class="flex-sidefill-r"></div>
</main>

<!-- cookie banner kindly provided by analytics.uni.lu -->
<div id="lap-cookies-banner">
  <div class="banner-intro">
    <div class="close-button">
      <a class="lap-refuse">×</a>
    </div>
    <div class="banner-img">
      <img src="https://analytics.lcsb.uni.lu/lap/static/logos/r3-logo.svg" type="image/svg+xml">
    </div>
    <div class="banner-title">This website needs some cookies and similar means to function.</div>
    <div class="banner-text">If you permit us, we will use those means to collect data on your visits for aggregated statistics to improve our service.</div>
  </div>
  <div class="banner-buttons">
    <a class="btn lap-accept">Accept cookies for aggregated statistics</a>
    <a class="btn lap-refuse">No thanks, only technically necessary cookies</a>
    <a class="btn lap-cookies-more" href="/website-test/privacy-policy">More information</a>
  </div>
</div>
</body>
</html>
