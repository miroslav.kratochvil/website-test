<html>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    HowTo Cards: Frozen pages
  </title>
  <link rel="stylesheet" href="/website-test/style.css?build_nonce=build-1701769098" />
  <link rel="stylesheet" href="/website-test/fonts.css?build_nonce=build-1701769098" />
  <script>
  // settings for the provided by analytics.uni.lu (that drives the cookie banner)
  const settings = {
    expires: "180",
    matomoURL: "https://analytics.lcsb.uni.lu/hub/",
    siteID: "3",
    accept_all_text: "Aggregate statistics cookies accepted",
    only_necessary_text: "Only necessary cookies accepted",
    cookieName: "lap",
    bots: /bot|crawler|spider|crawling/i,
    timeout_hidebanner: "500",
    cookieDomain: window.location.hostname,
  }
  </script>
  <script src="https://analytics.lcsb.uni.lu/lap/static/js/lap.js"></script>
  <link rel="stylesheet" type="text/css" href="https://analytics.lcsb.uni.lu/lap/static/css/lap.css">
</head>
<body>
<header>
  <div class="flex-sidefill-l topbar-borderbottom"></div>
  <div class="width-sidebox-counterweight topbar-borderbottom"></div>
  <div class="width-content same-height">
    <div class="topbar-padding topbar-borderbottom"></div>
    <div class="topbar-unilu topbar-borderbottom">
      <a href="/website-test/"><div class="logo-unilu"></div></a>
    </div>
    <div class="topbar-text topbar-borderbottom">
      <div class="header-title">How-to Cards</div>
      <div class="header-subtitle">The practical and handy reference</div>
    </div>
    <div class="flex-fill topbar-borderbottom"></div>
    <div class="topbar-lcsb topbar-borderbottom">
      <div class="logo-lcsb"></div>
    </div>
    <div class="topbar-padding topbar-borderbottom"></div>
  </div>
  <div class="width-sidebox-placeholder topbar-borderbottom"></div>
  <div class="flex-sidefill-r topbar-borderbottom"></div>
</header>
<main>
  <div class="flex-sidefill-l"></div>
  <div class="content-holder">
    <div class="sidebox">
      <div class="sidebox-right">
        <a href="/website-test/">Home</a>
        |
        <a href="/website-test/tag">All categories</a>
        |
        <a href="/website-test/search">Search</a>
      </div>
      <div class="sidebox-header">
    Card categories
  </div>
  <div class="sidebox-values">
    <ul>
    <li class="sidebox-tag"><a href="/website-test/tag/publication/process"> » All » Publication » Publication processes</a></li>
    </ul>
  </div>
  <div class="sidebox-header">
    Outline
  </div>
  <div class="sidebox-values sidebox-toc">
    <ul>
<li><a href="#frozen-pages" id="toc-frozen-pages">Frozen pages</a>
<ul>
<li><a href="#using-the-frozen-pages" id="toc-using-the-frozen-pages">Using the frozen pages</a>
<ul>
<li><a href="#when-is-the-content-of-the-frozen-page-accessible-to-the-public" id="toc-when-is-the-content-of-the-frozen-page-accessible-to-the-public">When is the content of the frozen page accessible to the public?</a></li>
</ul></li>
<li><a href="#modifying-the-frozen-pages" id="toc-modifying-the-frozen-pages">Modifying the frozen pages</a></li>
</ul></li>
</ul>
  </div>
  <div class="sidebox-header">
    Last modification
  </div>
  <div class="sidebox-values">
    2023-10-23
  </div>
  
  <div class="sidebox-header">
    Update the card in GitLab
  </div>
  <div class="sidebox-values">
    <var>lcsb/howto/internal</var> → <a href="https://gitlab.lcsb.uni.lu/lcsb/howto/internal/-/blob/master/cards/publication-process/frozen/frozen.md"><var>cards/publication-process/frozen/frozen.md</var></a>
  </div>
  
    </div>
    <div class="content width-content">
    <h1 id="frozen-pages">Frozen pages<a href="#frozen-pages" class="header-local-anchor" title="Link to this section">#</a></h1>
<p>Frozen pages collect supplementary information and references for each
publication generated at LCSB. They serve two main purposes:</p>
<ul>
<li>Each frozen page gets a <em>stable URL</em>. All frozen pages come with a generated
<em>Digital Object Identifier (DOI)</em>, which ensures that the frozen page will be
available under that identifier in the long term, thus making it a
sustainable source of information.</li>
<li>Despite the name, the “frozen” page can be easily updated by LCSB staff,
which allows you to e.g. change references to datasets in various cases that
necessitate this. As a typical use, you can fix “broken” links to programs,
data, sample repositories, and protocols in the frozen page. These are
typically hard or impossible to fix in the published manuscript.</li>
</ul>
<p>The frozen pages are maintained via
<a href="https://gitlab.lcsb.uni.lu/lcsb/frozen">LCSB GitLab, in namespace <code>lcsb/frozen</code></a>,
and published as a part of <a href="https://r3lab.uni.lu/">R³</a> website, at
<a href="https://r3lab.uni.lu/frozen/">r3lab.uni.lu/frozen</a>.</p>
<h2 id="using-the-frozen-pages">Using the frozen pages<a href="#using-the-frozen-pages" class="header-local-anchor" title="Link to this section">#</a></h2>
<p>In most cases, the frozen page will be created for you automatically during the
PPC process, with no action required from your side.</p>
<p>Manuscript authors are recommended to link the frozen page from the manuscript
using the following wording:</p>
<blockquote>
<p>References to supplementary data and code are additionally collected and maintained at https://doi.org/10.17881/xxxx-xxxx .</p>
</blockquote>
<p>The <code>xxxx-xxxx</code> in the DOI above <em>must</em> be replaced by the DOI of your frozen
page, such as
<a href="https://doi.org/10.17881/zkcr-bt30">https://doi.org/10.17881/zkcr-bt30</a>.
You can find your DOI in the PPC interface, and it is also available as a
“self-reference” from the actual frozen page.</p>
<p>Following additional considerations should apply:</p>
<ul>
<li>Reference to the frozen page should <strong>not</strong> replace proper linking of
supplementary data and code from the manuscript. Typically, you will have the
references in both manuscript (for concerns of editors and reviewers) and in
the frozen page (for long-term sustainability, and also easier access from
the internet by being more accessible for the search engines).</li>
<li>In some cases, the manuscript does not <em>seem</em> to require a frozen page (e.g.,
for pure reviews that do not use any data or code), and in turn, the
initially created frozen page seems quite empty. We still recommend to link
to the frozen page, for the cases when it would be viable to add any
supporting material in the future.</li>
</ul>
<h3 id="when-is-the-content-of-the-frozen-page-accessible-to-the-public">When is the content of the frozen page accessible to the public?<a href="#when-is-the-content-of-the-frozen-page-accessible-to-the-public" class="header-local-anchor" title="Link to this section">#</a></h3>
<p>Due to the expected confidentiality of the publications, the frozen pages <strong>are
not immediately public</strong> when your PPC is submitted. The visibility for others
progressively increases in the process, following these steps:</p>
<ol type="1">
<li>When a PPC is created, the PPC coaches create a preliminary version of the frozen page in the GitLab repository. At this point, the frozen page contents can be accessed only LCSB staff with access to this GitLab repository, and system administrators.</li>
<li>During the PPC process, the page is updated with additional information, such as links to data, programs, methods, repositories, preprints, etc., but stays accessible only by LCSB staff.</li>
<li>When the sustainability check of a publication is completed, the frozen page receives a DOI and becomes <strong>accessible from the internet only for the people who know the exact DOI</strong>. This allows you to submit a manuscript that links to the frozen page DOI, so that the frozen page can be accessed by reviewers and editors. At the same time, the information in the frozen page itself stays “link-locked” and inaccessible by the public.</li>
<li>After your manuscript passes PPC, gets reviewed and published in a journal or in proceedings, PPC coaches detect this via the monthly publication reports and make the page publicly accessible and findable.</li>
</ol>
<p>You are supposed to interfere with this process in two main cases:</p>
<ul>
<li><strong>You may want to completely prevent creation of frozen page due to high
confidentiality of the results.</strong> This usually happens with manuscripts which
are subject to technology transfer or patenting. PPC coaches will inquire on
the confidentiality and results of technology transfer check, thus avoiding
premature publication of any information via the frozen page. It is
recommended that you also <strong>explicitly declare any need for confidentiality
by stating it in the PPC sustainability check comments</strong>, for complete
clarity for everyone involved.</li>
<li>In some cases, <strong>you may want to make your frozen page publicly visible even
before it gets published with a manuscript</strong>, or before PPC coaches detect the
manuscript publication. In such cases, follow the “Modifying the frozen pages”
procedure below.</li>
</ul>
<h2 id="modifying-the-frozen-pages">Modifying the frozen pages<a href="#modifying-the-frozen-pages" class="header-local-anchor" title="Link to this section">#</a></h2>
<p>You can change any information on your frozen page simply by modifying it in
the appropriate repository in GitLab – currently, all frozen page contents
resides in project
<a href="https://gitlab.lcsb.uni.lu/lcsb/frozen/pages"><code>lcsb/frozen/pages</code></a>. README
file explains the precise contents and formatting of the repository contents.</p>
<p>For the confidentiality reasons mentioned above, the repository is not public.
You can ask for access via <a href="mailto:lcsb-r3@uni.lu">lcsb-r3@uni.lu</a>.</p>
<p>Alternatively, you can simply describe the required changes in a
<a href="https://service.uni.lu/sp?id=sc_cat_item&amp;sys_id=25d2b0b0dbef2c10ca53454039961940&amp;sysparm_category=c4dcecf4dbaf2c10ca5345403996196a">ServiceNow ticket for changing the frozen pages</a>.</p>
      <div class="content-footer">
        <div class="flex-fill"></div>
        <div class="footer-r3"></div>
        <div class="footer-lcsb"></div>
        <div class="footer-text">
          <div>
            The contents are available under <a href="/website-test/license">CC-BY-SA 4.0 LICENSE</a>.
          </div>
          <div>
            <a href="/website-test/privacy-policy">Privacy Policy</a>
            (<span id="gdpr-result-text"></span> - <a href="javascript:%20showBanner();">change</a>)
          </div>
        </div>
        <div class="flex-fill"></div>
      </div>
    </div>
    <div class="width-sidebox-counterweight"></div>
  </div>
  <div class="flex-sidefill-r"></div>
</main>

<!-- cookie banner kindly provided by analytics.uni.lu -->
<div id="lap-cookies-banner">
  <div class="banner-intro">
    <div class="close-button">
      <a class="lap-refuse">×</a>
    </div>
    <div class="banner-img">
      <img src="https://analytics.lcsb.uni.lu/lap/static/logos/r3-logo.svg" type="image/svg+xml">
    </div>
    <div class="banner-title">This website needs some cookies and similar means to function.</div>
    <div class="banner-text">If you permit us, we will use those means to collect data on your visits for aggregated statistics to improve our service.</div>
  </div>
  <div class="banner-buttons">
    <a class="btn lap-accept">Accept cookies for aggregated statistics</a>
    <a class="btn lap-refuse">No thanks, only technically necessary cookies</a>
    <a class="btn lap-cookies-more" href="/website-test/privacy-policy">More information</a>
  </div>
</div>
</body>
</html>
