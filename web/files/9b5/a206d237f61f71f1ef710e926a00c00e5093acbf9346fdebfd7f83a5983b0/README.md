This README file was generated on [YYYY-MM-DD] by [NAME]

GENERAL INFORMATION

Dataset title:

Description: <provide description of the dataset origin, steps used in its generation, content and its pupose>

Folder structure: <describe each folder and data within>

Date of data collection: <provide single date, range, or approximate date; suggested format YYYY-MM-DD>

Internal catalog ID (DAISY):

DOI to publication landing page (Frozen Page):

 <If the page behind DOI does not contain information about the dataset authors, usage license or guide how to cite it, please include it here.>